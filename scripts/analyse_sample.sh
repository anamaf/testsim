if [ "$#" -eq 1 ]
then
    sample=$1
    echo "Running FastQC..."
    mkdir -p out/fastqc
    fastqc -o out/fastqc data/$sample*.fastq.gz
    echo "Análisis fastqc finalizado"
    echo "Running cutadapt..."
    mkdir -p log/cutadapt
    mkdir -p out/cutadapt
    cutadapt -m 20 -a AGATCGGAAGAGCACACGTCTGAACTCCAGTCA -A AGATCGGAAGAGCGTCGTGTAGGGAAAGAGTGT -o out/cutadapt/${sample}_1.trimmed.fastq.gz -p out/cutadapt/${sample}_2.trimmed.fastq.gz data/${sample}_1.fastq.gz data/${sample}_2.fastq.gz > log/cutadapt/${sample}.log
    echo "Eliminación de los adaptadores finalizada"
    echo "Running STAR alignment..."
    mkdir -p out/star/$sample
    STAR --runThreadN 4 --genomeDir res/genome/star_index/ --readFilesIn out/cutadapt/${sample}_1.trimmed.fastq.gz out/cutadapt/${sample}_2.trimmed.fastq.gz --readFilesCommand zcat --outFileNamePrefix out/star/${sample}/   
    echo "Alineamiento finalizado"
else
    echo "Usage: $0 sample"
    exit 1
fi


