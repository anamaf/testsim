export WD =$(pwd) 
cd $WD
mkdir -p res/genome
echo "Descargando la secuencia genómica"
wget -O res/genome/ecoli.fna.gz ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/005/845/GCF_000005845.2_ASM584v2/GCF_000005845.2_ASM584v2_genomic.fna.gz
gunzip -k res/genome/ecoli.fna.gz
echo "Descarga del genoma finalizada"
echo "Running STAR index..."
mkdir -p res/genome/star_index
STAR --runThreadN 4 --runMode genomeGenerate --genomeDir res/genome/star_index/ --genomeFastaFiles res/genome/ecoli.fna --genomeSAindexNbases 9
echo "Indice del genoma realizado"

mkdir -p out/fastqc
mkdir -p log/cutadapt
mkdir -p out/cutadapt
mkdir -p out/multiqc
mkdir -p out/multiqc/multiqc_data
cd $WD
for sample in $(ls data/*.fastq.gz | cut -d"_" -f1 | sed "s:data/::" | sort | uniq)
do
	bash scripts/analyse_sample.sh $sample
done
multiqc -o out/multiqc $WD
echo "programa finalizado"
